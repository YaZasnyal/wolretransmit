#include "http_server.h"

#include <iostream>
#include <sstream>

#include <Poco/JSON/Parser.h>

#include "wol_sender.h"

WakeHandler::WakeHandler(WolSender& sender) : sender_(sender)
{

}

void WakeHandler::handleRequest(Poco::Net::HTTPServerRequest& request, Poco::Net::HTTPServerResponse& response)
{
    response.setStatusAndReason(Poco::Net::HTTPResponse::HTTP_OK, "200: Ok");
    try
    {
        std::string data;
        data.resize(request.getContentLength());
        request.stream().read(const_cast<char*>(data.data()), request.getContentLength());
        std::cout << data << std::endl;

        Poco::JSON::Parser parser;
        auto parseResult = parser.parse(data);
        sender_.Send(parseResult.extract<Poco::JSON::Object::Ptr>()->getValue<std::string>("mac"));
    }
    catch (std::exception& e)
    {
        std::cerr << "WakeHandler::handleRequest: Exception: " << e.what() << std::endl;
    }

    auto& ostr = response.send();
    ostr << "200: Ok";
    ostr.flush();
}

HttpFactory::HttpFactory(WolSender& sender) : sender_(sender)
{
    sender;
}

Poco::Net::HTTPRequestHandler* HttpFactory::createRequestHandler(const Poco::Net::HTTPServerRequest& request)
{
    if (request.getMethod() != Poco::Net::HTTPRequest::HTTP_POST)
        return nullptr;
    return new WakeHandler(sender_);
}
