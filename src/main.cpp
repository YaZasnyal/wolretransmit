#include <iostream>

#include <Poco/Util/ServerApplication.h>

#include "http_server.h"
#include "wol_sender.h"
#include "udp_server.h"

class Server : public Poco::Util::ServerApplication
{
    // Application interface
protected:
    int main(const std::vector<std::string>& args)
    {
        WolSender sender(9);

        UdpServer udpSrv(26000, sender);
        udpSrv.Start();

        Poco::Net::HTTPServer server(Poco::SharedPtr<HttpFactory>(new HttpFactory(sender)), 25023);
        server.start();

        waitForTerminationRequest();
        server.stopAll();
        udpSrv.Stop();

        return Poco::Util::ServerApplication::EXIT_OK;
    }
};

POCO_SERVER_MAIN(Server)
