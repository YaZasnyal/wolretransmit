#pragma once

#include <array>
#include <cstdint>
#include <string>

static int MacSize;
#pragma pack(1)
struct MagicPacket
{
    using MacType = std::array<uint8_t, 6>;
    MagicPacket(const std::string& mac);
    std::string GetMac() const;

    MacType preamble_;
    std::array<MacType, 16> mac_;
};
#pragma pack(0)

class WolSender
{
public:
    WolSender(uint16_t port);
    ~WolSender();

    void Send(const MagicPacket& packet);
    void Send(const std::string mac);

private:
    struct Impl;
    Impl* impl_;
};
