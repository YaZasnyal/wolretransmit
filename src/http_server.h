#pragma once

#include <Poco/Net/HTTPServer.h>
#include <Poco/Net/HTTPRequestHandler.h>
#include <Poco/Net/HTTPServerRequest.h>
#include <Poco/Net/HTTPServerResponse.h>

class WolSender;

struct WakeHandler : public Poco::Net::HTTPRequestHandler
{
    WakeHandler(WolSender& sender);
    // HTTPRequestHandler interface
public:
    void handleRequest(Poco::Net::HTTPServerRequest& request, Poco::Net::HTTPServerResponse& response);

private:
    WolSender& sender_;
};

struct HttpFactory : public Poco::Net::HTTPRequestHandlerFactory
{
    HttpFactory(WolSender& sender);
    // HTTPRequestHandlerFactory interface
public:
    Poco::Net::HTTPRequestHandler* createRequestHandler(const Poco::Net::HTTPServerRequest& request);

private:
    WolSender& sender_;
};
