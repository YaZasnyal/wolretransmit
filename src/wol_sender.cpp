#include "wol_sender.h"

#include <iostream>
#include <sstream>

#include <Poco/NumberParser.h>
#include <Poco/ByteOrder.h>

#include <Poco/Net/MulticastSocket.h>
#include <Poco/Net/NetworkInterface.h>

MagicPacket::MagicPacket(const std::string& mac)
{
    Poco::UInt64 parsedMac = 0;
    if (!Poco::NumberParser::tryParseHex64(mac, parsedMac))
    {
        throw ("Unable to convert");
    }
#if defined(POCO_ARCH_BIG_ENDIAN)
#else
    parsedMac = Poco::ByteOrder::toNetwork(parsedMac) >> 16;
#endif

    for(int i = 0; i < sizeof(preamble_); ++i)
    {
        preamble_[i] = 0xFF;
    }
    for(auto& it : mac_)
    {
        for(int i = 0; i < 6; ++i)
        {
            it[i] = reinterpret_cast<const uint8_t*>(&parsedMac)[i];
        }
    }
}

std::string MagicPacket::GetMac() const
{
    std::stringstream result;
    for(int i = 0; i < 6; ++i)
    {
        result << std::hex << (int)(reinterpret_cast<const uint8_t*>(&mac_[0])[i]) << ":";
    }
    return result.str();
}

struct WolSender::Impl
{
    void SendWol(const MagicPacket& pakcet);
    uint16_t port_;
    Poco::Net::DatagramSocket socket;
};

WolSender::WolSender(uint16_t port)
{
    impl_ = new Impl();
    impl_->port_ = port;
    impl_->socket = Poco::Net::DatagramSocket(
                        Poco::Net::SocketAddress(Poco::Net::IPAddress(), port));
}

WolSender::~WolSender()
{
    delete impl_;
}

void WolSender::Send(const MagicPacket& packet)
{
    impl_->SendWol(packet);
}

void WolSender::Send(const std::string mac)
{
    try {
        auto packet = MagicPacket(mac);
        impl_->SendWol(packet);
    } catch (const Poco::SyntaxException& e) {
        return;
    }
}

void WolSender::Impl::SendWol(const MagicPacket& packet)
{
    using namespace Poco::Net;
    std::cout << "WolSender::Impl::SendWol: Sending packet: " << packet.GetMac() << std::endl;
    if (!socket.getBroadcast())
        socket.setBroadcast(true);
    
    try
    {
        auto interfaces = NetworkInterface::map();
        for(auto& interface : interfaces)
        {
            auto addresses = interface.second.addressList();
            for(auto& address : addresses)
            {
                auto ip = address.get<NetworkInterface::BROADCAST_ADDRESS>();
                if (ip != IPAddress())
                {
                    std::cout << "WolSender::Impl::SendWol: sending to " << ip.toString() << std::endl;
                    socket.sendTo(reinterpret_cast<const char*>(&packet), sizeof (MagicPacket), SocketAddress(ip, port_));
                }
            }
        }
    }
    catch (std::exception& e)
    {
        std::cerr << "WolSender::Impl::SendWol: Exception: " << e.what() << std::endl;
    }
}
