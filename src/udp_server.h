#pragma once

#include <cstdint>

class WolSender;

class UdpServer
{
public:
    UdpServer(uint16_t port, WolSender& sender);
    ~UdpServer();

    void Start();
    void Stop();

private:
    struct Impl;
    Impl* impl_;
};
