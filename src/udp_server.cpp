#include "udp_server.h"

#include <iostream>
#include <thread>
#include <Poco/Net/DatagramSocket.h>

#include "wol_sender.h"

struct UdpServer::Impl
{
    Impl(WolSender& sender) : sender(sender), stop(false) {}
    ~Impl();
    void Run();

    uint16_t port_;
    WolSender& sender;
    std::thread thread;
    Poco::Net::DatagramSocket socket;
    volatile bool stop;
};

UdpServer::UdpServer(uint16_t port, WolSender& sender)
{
    impl_ = new Impl(sender);
    impl_->port_ = port;
}

UdpServer::~UdpServer()
{
    if (impl_)
        delete impl_;
}

void UdpServer::Start()
{
    impl_->Run();
}

void UdpServer::Stop()
{
    delete impl_;
    impl_ = nullptr;
}

UdpServer::Impl::~Impl()
{
    stop = true;
    socket.close();
    if (thread.joinable())
        thread.join();
}

void UdpServer::Impl::Run()
{
    thread = std::thread([&]() {
        using namespace Poco::Net;
        std::cout << "UdpServer::Impl::Run: Initialize socket" << std::endl;
        socket = DatagramSocket(SocketAddress(port_), true);
        std::string buffer; buffer.resize(65535);
        while(!stop)
        {
            if (!socket.poll(Poco::Timespan(250000), Socket::SELECT_READ | Socket::SELECT_ERROR))
                continue;
            std::cout << "UdpServer::Impl::Run: Recieved packet" << std::endl;
            auto bytes = socket.receiveBytes(const_cast<char*>(buffer.data()), buffer.size());
            for(int i = 0; i < bytes / sizeof (MagicPacket); ++i)
            {
                auto& packet = reinterpret_cast<const MagicPacket*>(buffer.data())[i];
	            std::cout << "UdpServer::Impl::Run: Sending packet: " << packet.GetMac() << std::endl;
	            sender.Send(packet);
            }
        }
    });
}
